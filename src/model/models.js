import Model from "redux-orm/es/Model";
import {attr, fk, many} from "redux-orm/es/fields";

export class Product extends Model {}

Product.fields = {
  id: attr(),
  name: attr(),
  kg: attr(),
  width: attr(),
  height: attr(),
  depth: attr()
};

Product.modelName = 'Product';

export class Address extends Model {}

Address.fields = {
  id: attr(),
  province: attr(),
  locality: attr(),
  zip: attr(),
  street: attr(),
  number: attr(),
  commentary: attr(),
};

Address.modelName = 'Address';

export const addressToString = (address) => `${address.street} ${address.number}, ${address.locality}, ${address.province}`;

export class Company extends Model {}

Company.fields = {
  id: attr(),
  name: attr(),
};

Company.modelName = 'Company';


export class Shipment extends Model {}

Shipment.modelName = 'Shipment';

Shipment.fields = {
  id: attr(),
  product: fk(Product.modelName, 'shipment'),
  origin: fk(Address.modelName, 'shipmentOrigin'),
  destination: fk(Address.modelName, 'shipmentDestination'),
  company: fk(Company.modelName, 'shipment'),
  status: attr(),
  cost: attr()
};
