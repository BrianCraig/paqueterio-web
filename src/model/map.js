export default class ImmutableMap {
    constructor(obj = {}){
        Object.entries(obj).forEach(([key, value]) => this[key] = value)
    }

    set(key, value) {
        const obj = {...this, [key]: value};
        return new this.constructor(obj)
    }

    remove(key) {
        const obj = {...this};
        delete obj[key];
        return new this.constructor(obj)
    }
}
