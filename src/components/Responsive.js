import React from "react";

let isMobile = () => window.innerWidth <= 768;

export const ActualResponsiveState = () => ({
  mobile: isMobile(),
  size: isMobile() ? "small" : "default"
});

export const defaultResponsiveContext = ActualResponsiveState();

export const ResponsiveContext = React.createContext(defaultResponsiveContext);

