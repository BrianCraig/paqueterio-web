import React from 'react'
import { hot } from 'react-hot-loader'
import BrowserRouter from "react-router-dom/es/BrowserRouter";
import Route from 'react-router-dom/es/Route'
import Header from './layout/Header'
import Home from './section/Home'
import { sections } from '../config/sections'
import Boxes from './layout/Boxes'
import Content from './layout/Content';
import {ResponsiveProvider} from "react-responsive-context";

import './App.sass'

class App extends React.PureComponent {

  sectionRender  = ({match}) => {
    const section = sections.find((c) => match.params.slug === c.slug);
    return (
      <Content name={section.slug}>
        <div id={"section"} className={`section-${section.slug}`}>
          <section.component/>
        </div>
      </Content>
    )
  }

  render = () => {
    return (
      <div>
        <ResponsiveProvider>
          <Boxes />
          <BrowserRouter basename={APP_BASENAME_ENV}>
            <div id="container">
              <Header section={this.section && this.section.name} />
              <Route exact path="/" component={Home} />
              <Route exact path="/:slug" render={this.sectionRender} />
            </div>
          </BrowserRouter>
        </ResponsiveProvider>
      </div>
    )
  };

}

export default hot(module)(App)