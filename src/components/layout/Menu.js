import React from "react";
import Tooltip from 'antd/es/tooltip/index';
import "../../assets/font-shopping/_flaticon.scss";
import "./Menu.sass";
import { sections } from '../../config/sections'
import Link from 'react-router-dom/es/Link'

export default class Menu extends React.PureComponent {

  render() {
    return (
      <div id={"menu"}>
        {
          sections.map((section) => (
            <Link to={section.slug} key={section.slug}>
              <Tooltip placement="right" title={section.name}>
                <p className={"fi flaticon-" + section.icon}/>
              </Tooltip>
            </Link>
          ))
        }
      </div>
    )
  }
}