import React from 'react'
import Button from 'antd/es/button/button'
import Modal from 'antd/es/modal/Modal'
import Input from 'antd/es/input/Input'
import Icon from 'antd/es/icon/index'
import { store } from "../../redux/store"
import { LogInAction, LogOutAction } from "../../redux/actions";
import connect from 'react-redux/es/connect/connect'
import Avatar from 'antd/es/avatar/index'

import "./Session.sass"

import compose from "redux/src/compose";
import withRouter from 'react-router-dom/es/withRouter';

@compose(
  withRouter,
  connect((store) =>({user: store.user}))
)
export default class Session extends React.PureComponent {
  state = {
    modalOpen: false,
    modalFetching: false,
    modalSelected: ""
  }

  openLogin = () => {
    this.setState({modalOpen: true, modalSelected: "Ingresar"})
  }

  openRegister = () => {
    this.setState({modalOpen: true, modalSelected: "Registrarse"})
  }

  logOut = () => {
    store.dispatch(new LogOutAction().asAction)
    this.props.history.push("/")
  }

  handleOk = () => {
    this.setState({
      modalFetching: true,
    });
    setTimeout(() => {
      this.setState({
        modalOpen: false,
        modalFetching: false,
      });
      store.dispatch(new LogInAction({nick: "Bri"}).asAction)
      this.props.history.push("/shipping")
    }, 2000);
  }

  onChangeUserName = (e) => {
    this.setState({ userName: e.target.value });
  }

  handleCancel = () => {
    console.log('Clicked cancel button');
    this.setState({
      modalOpen: false,
    });
  }

  ButtonList = () =>
    <React.Fragment>
      <Button onClick={this.openLogin} ghost>Ingresar</Button>
      <Button onClick={this.openRegister} ghost>Registrarse</Button>
    </React.Fragment>

  Profile = () =>
    <React.Fragment>
      <Avatar icon="user" />
      <p className="hide-on-mobile">Bienvenido, {this.props.user.nick} !</p>
      <Button ghost onClick={this.logOut}>Salir</Button>
    </React.Fragment>

  render () {
    return (
      <div>
        <div id={"session-buttons-list"}>{this.props.user ? <this.Profile/> : <this.ButtonList/>}</div>
        <Modal title={this.state.modalSelected}
               visible={this.state.modalOpen}
               confirmLoading={this.state.modalFetching}
               onOk={this.handleOk}
               onCancel={this.handleCancel}
        >
          <Input style={{marginBottom:16}} prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} onChange={this.onChangeUserName} placeholder="Usuario" />
          <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Clave" />
        </Modal>
      </div>
    )
  }
}
