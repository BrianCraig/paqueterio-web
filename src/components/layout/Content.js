import React from "react";
import Menu from './Menu'
import "./Content.sass"

export default ({children}) => {
  return (
    <div id="content">
      <Menu/>
      { children }
    </div>
  )
}