import React from 'react'

import Session from './Session'
import "./Header.sass"

export default class Header extends React.PureComponent {
  render () {
    return (
      <div id="header">
        <div className="title">Paqueter.io<span>{this.props.section}</span></div>
        <div className="session-container">
          <Session router={this.props.router}/>
        </div>
      </div>
    )
  }
}

Header.defaultProps = {
    section: ""
};
