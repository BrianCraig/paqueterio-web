import React from "react";
import "./Boxes.sass";

export default () => {
  return (
    <div className={"boxes"}>
      <div title="base" />
      <div title="complement1"/>
      <div title="complement2"/>
      <div title="darken1"/>
      <div title="darken2"/>
      <div title="lighter1"/>
      <div title="lighter2"/>
    </div>
  )
}