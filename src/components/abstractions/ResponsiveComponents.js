import React from "react";
import {ResponsiveConsumer} from "react-responsive-context";
import Button from "antd/es/button/button";
import Table from "antd/es/table/Table";
import Search from "antd/es/input/Search";
import Input from "antd/es/input/Input";

const toSize = ({mobile}) => mobile ? "small" : "default";

const wrap = (node) => <ResponsiveConsumer>{(responsive) =>
  node(responsive)
}</ResponsiveConsumer>;

export const ResponsiveButton =
  (props) => wrap(responsive => <Button {...props} size={toSize(responsive)} icon={responsive.mobile ? null : props.icon}/>);

export const ResponsiveSearch =
  (props) => wrap(responsive => <Search {...props} size={toSize(responsive)}/>);


const tableColumnProps = ({columns, mobileColumns}, {mobile}) => (mobileColumns && mobile) ? mobileColumns : columns;

export const ResponsiveTable =
  (props) => wrap(responsive => <Table {...props} size={toSize(responsive)} columns={tableColumnProps(props, responsive)}/>);

export const ResponsiveInput =
  (props) => wrap(responsive => <Input {...props} size={toSize(responsive)}/>);
