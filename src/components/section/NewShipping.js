import React from "react";
import unity from "../../config/unity";
import ButtonGroup from "antd/es/button/button-group";
import NewShippingProduct from "./modules/NewShippingProduct";
import {OriginForm, DestinationForm} from "./modules/AddressFrom";
import CarrierForm from "./modules/CarrierForm";
import {ResponsiveButton} from "../abstractions/ResponsiveComponents";

const sections = [
    {
        id:"product",
        name: "Producto",
        component: NewShippingProduct,
        icon: "gift"
    },
    {
        id: "origin",
        name: "Origen",
        component: OriginForm,
        icon: "shop"
    },
    {
        id: "destination",
        name: "Destino",
        component: DestinationForm,
        icon: "bell"
    },
    {
        id: "carrier",
        name: "Envío",
        component: CarrierForm,
        icon: "schedule"
    },
];

const requiredSections = [sections[0], sections[1], sections[2]];

const carrierSection = sections[3];

export default class NewShipping extends React.PureComponent {

    state = {section: sections[0]};

    sectionForm;

    changeSection = (s) => {

        const form = this.sectionForm.props.form;
        new Promise(function(resolve) {form.validateFields(null, {}, resolve)})
            .then((errors, values) => {
                if(errors !== null) return;
                this.setState((prevState) => ({
                    [prevState.section.id]: form.getFieldsValue(),
                    section: s
                }));
            });
    };

    componentDidUpdate = () => {
        if(this.state[this.state.section.id]){
            this.sectionForm.props.form.setFieldsValue(this.state[this.state.section.id]);
        }
    };

    static getDerivedStateFromProps(props, state){
        let predicate = (s) => state[s.id] === undefined;
        if(state.section === carrierSection && requiredSections.some(predicate)){
            return {
                ...state,
                section: requiredSections.find(predicate)
            }
        }
        return null;
    }

    render() {
        return <React.Fragment>
            <ButtonGroup style={{height: unity.Filter, marginBottom: unity.Margin, display: "flex"}}>
                { sections.map(s => <ResponsiveButton
                    ghost={this.state.section !== s}
                    key={s.icon}
                    type="primary"
                    icon={s.icon}
                    onClick={()=>this.changeSection(s)}
                    style={{flexGrow: 1}}>{s.name}</ResponsiveButton>)
                }
            </ButtonGroup>
            <div style={{flexGrow: 1}}>
                <this.state.section.component wrappedComponentRef={(inst) => this.sectionForm = inst}/>
            </div>
        </React.Fragment>
    }
}