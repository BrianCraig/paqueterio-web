import React from 'react'
import Row from 'antd/es/grid/row'
import Col from 'antd/es/grid/col'
import Card from 'antd/es/card/index'
import './Home.sass'

export default class Home extends React.PureComponent {
  ProvidersExample = () =>
    <div>
      <Row gutter={16}>
        <Col span={8}>
          <Card loading actions={["elegir"]} title="Proveedor A" style={{boxShadow: "rgb(12, 214, 0) 0px 0px 4px"}} extra={<span>$115</span>}>
            c
          </Card>
        </Col>
        <Col span={8}>
          <Card loading actions={["elegir"]} title="Proveedor B" extra={<span>$130</span>}>
            c
          </Card>
        </Col>
        <Col span={8}>
          <Card loading actions={["elegir"]} title="Proveedor C" extra={<span>$135</span>}>
            c
          </Card>
        </Col>
      </Row>
    </div>

  render () {
    return (
      <div id={"home"}>
        <div id="ship-without-hassle">
          <h1>Ship without hassle</h1>
        </div>
        <p>En paqueter.IO nos encargamos mostrarte todas las opciones y todos los precios de los envíos, para que compares y eligas el mejor distribuidor!
          Usando nuestra plataforma, puedes tener acceso a múltiples servicios de envío en uno solo! Disponible para pequeños y grandes volumenes</p>
        <this.ProvidersExample />
      </div>
    )
  }
}
