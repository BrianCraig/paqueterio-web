import React from "react";
import {AddProductAction} from "../../redux/actions";
import {store} from "../../redux/store";

export default class Integrations extends React.PureComponent {
  render() {
    return (
      <React.Fragment>
        <h1>Integraciones</h1>
        <h2>H2 Lorem ipsum dolor sit amet</h2>
        <h3>H3 Lorem ipsum dolor sit amet</h3>
        <h4 onClick={() => store.dispatch(new AddProductAction().asAction)}>
          H4 Lorem ipsum dolor sit amet</h4>
        <p>p Lorem ipsum dolor sit amet</p>
        <p>p <b>Bold</b> dolor <u>underlined</u></p>
      </React.Fragment>
    )
  }
}