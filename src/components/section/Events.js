import React from "react";
import Table from "antd/es/table/Table";
import {ResponsiveTable} from "../abstractions/ResponsiveComponents";

const repeat = (func, times) => {
    func(times);
    --times && repeat(func, times);
}

const data = [];
repeat((x) => {
    data.push({
        key: x,
        receiver: 'Juana del campo',
        startedOn: '24/04/2018',
        price: 120.5,
        carrier: "Oca",
        message: "El producto fue recibido en el destino",
        expectedReceiveOn: '28/04/2018',
        status: "Esperando retiro"
    })
}, 20);

const columns = [{
    title: 'Para',
    dataIndex: 'receiver',
    key: 'receiver'
}, {
    title: 'Fecha',
    dataIndex: 'startedOn',
    key: 'startedOn',
}, {
    title: 'Mensaje',
    dataIndex: 'message',
    key: 'message',
}];

export default class Events extends React.PureComponent {


  render() {
    return <React.Fragment>
      <ResponsiveTable
             className="ok"
             columns={columns}
             dataSource={data}
             pagination={false}/>
    </React.Fragment>

  }
}