import React from "react";
import FormItem from "antd/es/form/FormItem";
import Input from "antd/es/input/Input";
import Form from "antd/es/form/Form";
import Button from "antd/es/button/button";

@Form.create()
export default class CarrierForm extends React.PureComponent {
    render() {
        return <Form style={{maxWidth: 400, margin: "0 auto"}}>
            <FormItem>
                {this.props.form.getFieldDecorator('piola',{
                    rules: [{
                        required: true, message: 'Este campo es requerido',
                    }]
                })(
                    <Input type={"text"} placeholder={"en construcción"}/>,
                )}
            </FormItem>
            <FormItem>
                <Button disabled={true} style={{width: "100%"}}>Aceptar</Button>
            </FormItem>
        </Form>
    }
}