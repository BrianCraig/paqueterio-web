import React from "react";
import FormItem from "antd/es/form/FormItem";
import Input from "antd/es/input/Input";
import Form from "antd/es/form/Form";
import Select from "antd/es/select/index";

class AddressForm extends React.PureComponent {
    render() {
        return <Form style={{maxWidth: 400, margin: "0 auto"}}>
            <FormItem>
                {this.props.form.getFieldDecorator('province',{
                    rules: [{
                        required: true, message: 'Este campo es requerido',
                    }]
                })(
                    <Select
                        showSearch
                        placeholder="Provincia"
                        optionFilterProp="children"
                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                    >
                        <Option value="ba">Buenos Aires</Option>
                        <Option value="lp">La Pampa</Option>
                        <Option value="sl">San Luis</Option>
                    </Select>,
                )}
            </FormItem>
            <FormItem>
                {this.props.form.getFieldDecorator('locality',{
                    rules: [{
                        required: true, message: 'Este campo es requerido',
                    }]
                })(
                    <Input type="text" placeholder={"Localidad"}/>,
                )}
            </FormItem>
            <FormItem>
                {this.props.form.getFieldDecorator('zip',{
                    rules: [{
                        required: true, message: 'Este campo es requerido',
                    }]
                })(
                    <Input type="string" placeholder={"Codigo Postal"}/>
                )}
            </FormItem>
            <FormItem>
                {this.props.form.getFieldDecorator('street',{
                    rules: [{
                        required: true, message: 'Este campo es requerido',
                    }]
                })(
                    <Input type="string" placeholder={"Calle"} />
                )}
            </FormItem>
            <FormItem>
                {this.props.form.getFieldDecorator('number',{
                    rules: [{
                        required: true, message: 'Este campo es requerido',
                    }]
                })(
                    <Input type="number" placeholder={"Altura"}/>
                )}
            </FormItem>
            <FormItem>
                {this.props.form.getFieldDecorator('commentary')(
                    <Input type="string" placeholder={"Comentario"}/>
                )}
            </FormItem>
        </Form>
    }
}

export const OriginForm = Form.create()(AddressForm);
export const DestinationForm = Form.create()(AddressForm);