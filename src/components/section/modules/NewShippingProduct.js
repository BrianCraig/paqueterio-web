import React from "react";
import FormItem from "antd/es/form/FormItem";
import Input from "antd/es/input/Input";
import Form from "antd/es/form/Form";

@Form.create()
export default class NewShippingProduct extends React.PureComponent {

    render() {
        return <Form style={{maxWidth: 400, margin: "0 auto"}}>
            <FormItem>
                {this.props.form.getFieldDecorator('name',{
                    rules: [{
                        required: true, message: 'Este campo es requerido',
                    }]
                })(
                    <Input placeholder={"Nombre del producto"}/>
                )}
            </FormItem>
            <FormItem>
                {this.props.form.getFieldDecorator('kg',{
                    rules: [{
                        required: true, message: 'Este campo es requerido',
                    }]
                })(
                    <Input type="number" placeholder={"Peso"} addonAfter={"kg"}/>
                )}
            </FormItem>
            <FormItem>
                {this.props.form.getFieldDecorator('width',{
                    rules: [{
                        required: true, message: 'Este campo es requerido',
                    }]
                })(
                    <Input type="number" placeholder={"Ancho"} addonAfter={"cm"}/>
                )}
            </FormItem>
            <FormItem>
                {this.props.form.getFieldDecorator('height',{
                    rules: [{
                        required: true, message: 'Este campo es requerido',
                    }]
                })(
                    <Input type="number" placeholder={"Alto"} addonAfter={"cm"} />
                )}
            </FormItem>
            <FormItem>
                {this.props.form.getFieldDecorator('depth',{
                    rules: [{
                        required: true, message: 'Este campo es requerido',
                    }]
                })(
                    <Input type="number" placeholder={"Profundidad"} addonAfter={"cm"} />
                )}
            </FormItem>
        </Form>
    }
}