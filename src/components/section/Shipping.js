import React from "react";
import Table from 'antd/es/table/Table'
import "./Shipping.sass"
import Button from 'antd/es/button/button'
import Search from "antd/es/input/Search"
import connect from "react-redux/es/connect/connect";
import {shipmentsSelector} from "../../redux/selectors";
import Row from "antd/es/grid/row";
import Card from "antd/es/card/index";
import Col from "antd/es/grid/col";
import {addressToString} from "../../model/models";
import {ResponsiveConsumer} from "react-responsive-context";
import compose from "redux/src/compose";
import {ResponsiveButton, ResponsiveSearch, ResponsiveTable} from "../abstractions/ResponsiveComponents";

const productColumn = {
  title: 'Producto',
  dataIndex: 'product.name',
  key: 'pn'
};
const costColumn = {
  title: 'Costo',
  dataIndex: 'cost',
  key: 'cost',
};
const providerColumn = {
  title: 'Proveedor',
  dataIndex: 'company.name',
  key: 'carrier'
};
const statusColumn = {
  title: 'Estado',
  dataIndex: 'status',
  key: 'status'
};
const columns = [productColumn, costColumn, providerColumn, statusColumn];
const mobileColumns = [productColumn, statusColumn];
const toSize = mobile => mobile ? "small" : "default";

@compose(connect((state) => ({
    shipments: shipmentsSelector(state)
  }))
)
export default class Shipping extends React.PureComponent {
  tableSection = () =>
    <React.Fragment>
      <div className="top">
        <ResponsiveSearch placeholder={"Filtrar"}/>
        <ResponsiveButton disabled={!this.rowsSelected()}>Etiquetas</ResponsiveButton>
        <ResponsiveButton disabled={!this.rowsSelected()}
                          onClick={() => this.setState({section: this.detailSection})}>Detalles</ResponsiveButton>
      </div>
      <ResponsiveTable
        className={"table"}
        rowKey={"id"}
        columns={columns}
        mobileColumns={mobileColumns}
        dataSource={this.props.shipments}
        pagination={false}
        rowSelection={this.rowSelection}/>
    </React.Fragment>

  detailSection = () =>
    <Row gutter={16} style={{display: "flex", flexWrap: "wrap"}}>
      {this.state.selectedRows.map(shipment => <Col key={shipment.id} sm={{span: 24}} md={{span:8}} style={{marginBottom: 16}}>
        <Card title={shipment.product.name} extra={`$${shipment.cost}`} >
          <p>Origen: {addressToString(shipment.origin)}</p>
          <p>Destino: {addressToString(shipment.destination)}</p>
          <p>Compañia: {shipment.company.name}</p>
        </Card>
      </Col>)}
    </Row>;



  state = {
    selectedRows: [],
    section: this.tableSection
  };

  rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      this.setState({selectedRows});
    }
  };

  rowsSelected = () => this.state.selectedRows.length > 0;

  render() {
    return <React.Fragment>
      <this.state.section />
    </React.Fragment>
  }
}