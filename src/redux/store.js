import createStore from "redux/src/createStore";
import Store from "../model/store";
import orm from "./orm";
import {databaseMock} from "./database-mock";

const seed = {
    shipments: [],
    db: databaseMock(orm, orm.getEmptyState())
};

const reducer = (state, action) => {
    if(action.type === "ACTION") return action.execute(state);
    return state;
};

export const store = createStore(reducer, new Store(seed));
