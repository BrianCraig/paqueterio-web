import ORM from "redux-orm/es/ORM";
import {Address, Company, Product, Shipment} from "../model/models";

const orm = window.orm = new ORM();
orm.register(Product, Address, Shipment, Company);
export default orm;

