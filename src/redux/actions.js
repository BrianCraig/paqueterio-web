import User from "../model/user";
import orm from "./orm";

class Action {
  execute() {
    throw new Error("Plis do somthing madafaker")
  }

  get asAction() {
    return {
      type: "ACTION",
      execute: (store) => this.execute(store)
    }
  }
}

export class LogInAction extends Action {
  constructor(user) {
    super();
    this.user = new User(user)
  }

  execute(state) {
    return state.set("user", this.user);
  }
}

export class LogOutAction extends Action {
  execute(state) {
    return state.set("user", undefined);
  }
}

export class DBAction extends Action {
  dbAction(session) {
    throw new Error("Plis do somthing with tha session")
  }

  execute(state) {
    let session = orm.session(state.db);
    this.dbAction(session);
    return state.set("db", session.state);
  }
}

export class AddProductAction extends DBAction {
  dbAction(session) {
    session.Product.create({name: "Changos"})
  }
}



