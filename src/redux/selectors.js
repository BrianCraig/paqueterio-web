import orm from "./orm";
import {createSelector} from "redux-orm/es/redux";

const dbStateSelector = state => state.db;

export const shipmentsSelector = createSelector(
  orm,
  // The first input selector should always select the db-state.
  // Behind the scenes, `createSelector` begins a Redux-ORM session
  // with the value returned by `dbStateSelector` and passes
  // that Session instance as an argument instead.
  dbStateSelector,
  session => session.Shipment.all().toModelArray().map(m => ({
    ...m.ref,
    product: m.product.ref,
    origin: m.origin.ref,
    destination: m.destination.ref,
    company: m.company.ref
  }))
);