import faker from "faker/locale/es";
import times from "lodash-es/times";
import sample from "lodash-es/sample";
import random from "lodash-es/random";


const mockProduct = (sess) => sess.Product.create({
  name: faker.commerce.product(),
  kg: faker.random.number(5),
  width: faker.random.number(200),
  height: faker.random.number(200),
  depth: faker.random.number(50),
});

const mockAddress = (sess) => sess.Address.create({
  province: faker.address.state(),
  locality: faker.address.city(),
  zip: faker.address.zipCode(),
  street: faker.address.streetName(),
  number: faker.address.streetAddress(),
  commentary: faker.random.words(10)
});

const mockCompany = (sess) => sess.Company.create({
  name: faker.company.companyName(),
});

const mockShipment = (sess, prod, ori, dest, comp) => sess.Shipment.create({
  product: prod,
  origin: ori,
  destination: dest,
  company: comp,
  status: sample(["completed", "cancelled", "in transit to customer", "in transit to deposit", "ready to dispatch"]),
  cost: random(100, 300)
});



export const databaseMock = (orm, db) => {
  const session = orm.session(db);
  let products = times(20, () => mockProduct(session));
  let addresses = times(20, () => mockAddress(session));
  let company = times(8, () => mockCompany(session));
  let shipments = times(40, () => mockShipment(session, sample(products), sample(addresses), sample(addresses), sample(company)))
  return session.state;
}