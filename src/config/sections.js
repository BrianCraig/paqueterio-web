import Shipping from '../components/section/Shipping'
import NewShipping from '../components/section/NewShipping'
import Statistics from '../components/section/Statistics'
import Help from '../components/section/Help'
import Integrations from '../components/section/Integrations'
import Event from '../components/section/Events'

export const sections = [
  {
    name: "Envíos",
    slug: "shipping",
    component: Shipping,
    icon: "delivery-truck"
  },
  {
    name: "Eventos",
    slug: "events",
    component: Event,
    icon: "notification"
  },
  {
    name: "Nuevo envío",
    slug: "new-shipping",
    component: NewShipping,
    icon: "package"
  },
  {
    name: "Estadisticas",
    slug: "statistics",
    component: Statistics,
    icon: "piggy-bank"
  },
  {
    name: "Ayuda",
    slug: "help",
    component: Help,
    icon: "chat"
  },
  {
    name: "Integraciónes",
    slug: "integrations",
    component: Integrations,
    icon: "qr-code"
  },

]