const unit = 16;

export default{
    Unit: unit,
    Gutter: unit,
    Title: 4 * unit,
    Subtitle: 3 * unit,
    Filter: 2 * unit,
    Margin: unit
}