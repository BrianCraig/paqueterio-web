// We are using node's native package 'path'
// https://nodejs.org/api/path.html
const path = require('path')

const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const webpack = require("webpack")

// Constant with our paths
const paths = {
  DIST: path.resolve(__dirname, 'dist'),
  SRC: path.resolve(__dirname, 'src'),
  PUBLIC: path.resolve(__dirname, 'public')
}

const basename = process.env.APP_BASENAME || '/';
// Webpack configuration
module.exports = {
  entry: path.join(paths.SRC, 'index.js'),
  output: {
    path: paths.DIST,
    filename: 'app.bundle.[hash].js',
    publicPath: basename
  },
  devServer: {
    clientLogLevel: "warning",
    historyApiFallback: true,
    host: '0.0.0.0'
  },
  plugins: [
    //new (require('webpack-bundle-analyzer').BundleAnalyzerPlugin)(),
    new HtmlWebpackPlugin({
      template: path.join(paths.PUBLIC, 'index.html')
    }),
    new MiniCssExtractPlugin({
      filename: '[name].css'
    }),
    new webpack.NamedModulesPlugin(),
    new webpack.DefinePlugin({
      'APP_BASENAME_ENV': JSON.stringify(basename)
    }),
  ],
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: [
          'babel-loader'
        ]
      },
      {
        test: /\.(sass|scss)$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
      },
      {
        test: /\.(less)$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader', {
          loader:'less-loader',
          options: { javascriptEnabled: true }
        }]
      },
      {
        test: /\.(png|svg|jpg|gif|eot|ttf|woff)$/,
        use: ['file-loader']
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx']
  }
}
